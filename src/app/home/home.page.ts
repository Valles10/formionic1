import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  nombreIn: string;
  apellidoIn: string;
  telefonoIn: number;
  edadIn: number;
  fechaIn: string;

  constructor(private router: Router) {}
  goToVista(){
    this.router.navigate(["/vista" , this.nombreIn , this.apellidoIn , this.telefonoIn , this.edadIn , this.fechaIn]);
  }
  
}
