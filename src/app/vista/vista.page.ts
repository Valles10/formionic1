import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vista',
  templateUrl: './vista.page.html',
  styleUrls: ['./vista.page.scss'],
})
export class VistaPage implements OnInit {

  nom = null;
  ape = null;
  tel = null;
  eda = null;
  fec = null;

  constructor(private activatedRoute: ActivatedRoute) {

    
    
  }

  ngOnInit(){
    this.nom = this.activatedRoute.snapshot.paramMap.get("nom");
    this.ape = this.activatedRoute.snapshot.paramMap.get("ape");
    this.tel = this.activatedRoute.snapshot.paramMap.get("tel");
    this.eda = this.activatedRoute.snapshot.paramMap.get("eda");
    this.fec = this.activatedRoute.snapshot.paramMap.get("fec");
  }

}
